import torch
from torch import nn, optim
import torch.nn.functional as F
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms
from skimage import io
from PIL import Image
import FileLoader
from torch.utils import data
import numpy as np
import matplotlib.pyplot as plt
import os

def SavePic(EndMat,labelSet,st,participant,PicDimosion,Train=True):
    size = EndMat.shape[0]
    root = ""
    if Train==True:
        root = "Result/"
    else:
        root = "TestResult/"
    ImgDir = root + str(participant) + "/"
    if(not os.path.exists(ImgDir)):
        os.makedirs(ImgDir)
    File = root+"Mark.txt"
    f = open(File,'a+')

    for i in range(size):
        imgPath = ImgDir+str(st+i)+".jpg"
        img = EndMat[i,:]
        img = img.reshape(PicDimosion,-1)
        im=Image.fromarray(img)

        im = im.convert("L")
        im.save(imgPath)

        print(imgPath)

        f.write(imgPath+" "+str(labelSet[i])+'\n')

    f.close()


def SaveNpy(EndMat,labelSet,st,participant,PicDimosion,Train=True):
    size = EndMat.shape[0]
    root = ""
    if Train==True:
        root = "Result/"
    else:
        root = "TestResult/"
    ImgDir = root + str(participant) + "/"
    if(not os.path.exists(ImgDir)):
        os.makedirs(ImgDir)
    File = root+"Mark.txt"
    f = open(File,'a+')

    for i in range(size):
        imgPath = ImgDir+str(st+i)+".npy"
        img = EndMat[i,:]
        img = img.reshape(PicDimosion,-1)
        np.save(imgPath,img)

        f.write(imgPath+" "+str(labelSet[i])+'\n')

    f.close()

