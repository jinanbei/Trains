import torch
from torch import nn, optim
import torch.nn.functional as F
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms
from PIL import Image
import FileLoader
from torch.utils import data
import numpy as np
import matplotlib.pyplot as plt
import TransNetVLoader

batch_size = 100
TrainFileName = 'data/MNIST/raw/train.txt'
TestFileName = 'data/MNIST/raw/test.txt'
participantNum = 4
ImgDimonsion = 784

SelfMats = []
part = int(ImgDimonsion / participantNum)
for m in range(participantNum):
    SelfMat = np.random.normal(0, 1, (part, part))
    SelfMat = torch.from_numpy(SelfMat).float()
    SelfMats.append(SelfMat)
print("矩阵创建完毕")

# 下面是将原始图像变成 修改后的图像 只用一遍即可。

TransNetVLoader.TrainsNetVChange(FileName=TrainFileName, ImgDimonsion=ImgDimonsion, SelfMats=SelfMats,
                                 participantNum=participantNum,
                                 batch=batch_size, Train=True)

TransNetVLoader.TrainsNetVChange(FileName=TestFileName, ImgDimonsion=ImgDimonsion, SelfMats=SelfMats,
                                 participantNum=participantNum,
                                 batch=batch_size, Train=False)

print("图像转换完毕")